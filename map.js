// functions to display RENRT or SALE properties based on FILTER radio button
var filterSaleRent = "/api/map/rent"; // default = rent properties

function saleCheck() {
  filterSaleRent = "/api/map";
  var divCard = document.getElementById("mapcards");
  if (divCard) {
    while (divCard.firstChild) {
      divCard.removeChild(divCard.lastChild);
    }
    initMap();
  }
}

function rentCheck() {
  filterSaleRent = "/api/map/rent";
  var divCard = document.getElementById("mapcards");
  if (divCard) {
    while (divCard.firstChild) {
      divCard.removeChild(divCard.lastChild);
    }
    initMap();
  }
}

// function SEARCH bar
function searchCity() {
  var divCard = document.getElementById("mapcards");
  if (divCard) {
    while (divCard.firstChild) {
      divCard.removeChild(divCard.lastChild);
    }
    initMap();
  }
}

// function FILTER by TYPE OF PROPERTY
function isChecked() {
  var divCard = document.getElementById("mapcards");
  if (divCard) {
    while (divCard.firstChild) {
      divCard.removeChild(divCard.lastChild);
    }
  }
  initMap();
}

//function FILTER by price
function isPrice() {
  var divCard = document.getElementById("mapcards");
  if (divCard) {
    while (divCard.firstChild) {
      divCard.removeChild(divCard.lastChild);
    }
  }
  initMap();
}

var map;

// Initialize and add the map
function initMap() {
  // The location of Uluru
  const uluru = {
    lat: 45.525671,
    lng: -73.574905,
  };
  // The map, centered at Uluru
  map = new google.maps.Map(document.getElementById("map"), {
    zoom: 4,
    center: uluru,
  });
  // The marker, positioned at Uluru

  downloadUrl(filterSaleRent, function (data) {
    var xml = data.responseXML;
    var markers = xml.documentElement.getElementsByTagName("marker");

    Array.prototype.forEach.call(markers, function (markerElem) {
      var id = markerElem.getAttribute("id");
      var address = markerElem.getAttribute("address");
      var askingPrice = Number(markerElem.getAttribute("askingPrice"));
      var city = markerElem.getAttribute("city");
      var province = markerElem.getAttribute("province");
      var type = markerElem.getAttribute("propertyType");
      var point = new google.maps.LatLng(
        parseFloat(markerElem.getAttribute("lat")),
        parseFloat(markerElem.getAttribute("lng"))
      );

      var markerLocation = {
        id,
        askingPrice,
        address,
        city,
        province,
        point,
        type,
      };

      var infowincontent = document.createElement("div");
      var strong = document.createElement("strong");
      strong.textContent = address;
      infowincontent.appendChild(strong);
      infowincontent.appendChild(document.createElement("br"));

      // labels of markers
      var customLabel = {
        restaurant: {
          label: id, // change later for price
        },
        bar: {
          label: id,
        },
      };

      var text = document.createElement("text");
      text.textContent = address;

      infowincontent.appendChild(text);
      var icon = customLabel[type] || {};
      var marker = new google.maps.Marker({
        map: map,
        position: point,
        label: icon.label,
        opacity: 0.6,
      });

      var inputCity = document.getElementById("inputCity").value;
      console.log(inputCity);

      if (inputCity !== "") {
        if (inputCity === markerLocation.city) {
          validateFilter(markerLocation, marker);
        } else {
          // add action here -------------------------------------------- MISSING
        }
      }else {
        validateFilter(markerLocation, marker);
      }
    });
  });
}

function validateFilter(markerLocation, marker) {
  var minValue = Number(document.getElementById("priceMin").value);
  var maxValue = Number(document.getElementById("priceMax").value);
  var checkboxHouse = document.getElementById("houseChecked");
        if (
          checkboxHouse.checked &&
          markerLocation.type === "House" &&
          markerLocation.askingPrice > minValue &&
          markerLocation.askingPrice < maxValue
        ) {
          // BUILD HTML CARD
          buildCard(markerLocation, marker);
        }

        var checkboxCondo = document.getElementById("condoChecked");
        if (
          checkboxCondo.checked &&
          markerLocation.type === "Condo" &&
          markerLocation.askingPrice > minValue &&
          markerLocation.askingPrice < maxValue
        ) {
          buildCard(markerLocation, marker);
        }

        var checkboxApartment = document.getElementById("apartmentChecked");
        if (
          checkboxApartment.checked &&
          markerLocation.type === "Apartment" &&
          markerLocation.askingPrice > minValue &&
          markerLocation.askingPrice < maxValue
        ) {
          buildCard(markerLocation, marker);
        }

        var checkboxDuplex = document.getElementById("duplexChecked");
        if (
          checkboxDuplex.checked &&
          markerLocation.type === "Deuplex" &&
          markerLocation.askingPrice > minValue &&
          markerLocation.askingPrice < maxValue
        ) {
          buildCard(markerLocation, marker);
        }

        var checkboxTriplex = document.getElementById("triplexChecked");
        if (
          checkboxTriplex.checked &&
          markerLocation.type === "Triplex" &&
          markerLocation.askingPrice > minValue &&
          markerLocation.askingPrice < maxValue
        ) {
          buildCard(markerLocation, marker);
        }
}

function buildCard(markerLocation, marker) {
  // fetch div from html to be placed the cards
  var divCard = document.getElementById("mapcards");

  // creating card container
  var divCardContainer = document.createElement("div");
  divCardContainer.classList = ["card col col-sm-12 col-md-6 col-lg-4"];
  divCardContainer.style.width = "18rem";

  // creating card properties (image, title/price, description, link to the property)
  var imgPropCard = document.createElement("img"); // img - append to divCardContainer
  imgPropCard.src = "/images/" + markerLocation.id +"/" + markerLocation.id + "small.jpg";
  imgPropCard.classList = ["card-img-top "];
  imgPropCard.style = "border-radius: 20px"

  var divBodyCard = document.createElement("div"); // body of card - append to divCardContainer
  divBodyCard.classList = ["card-body"];

  var priceTitleCard = document.createElement("H5"); // price/title - append to divCardBody
  priceTitleCard.classList = ["card-title"];

  var addressCard = document.createElement("P"); // address of property - append to divCardBody
  addressCard.classList = ["card-text fs-6"];
  var cityCard = document.createElement("span"); // description of property - append to divCardBody
  cityCard.classList = ["card-text"];
  var provinceCard = document.createElement("P"); // description of property - append to divCardBody
  cityCard.classList = ["card-text"];

  var btnPropDetailsCard = document.createElement("A"); // link heref/btn to property details page - append to divCardBody
  btnPropDetailsCard.classList = ["btn btn-primary"];
  btnPropDetailsCard.textContent = "See Details";
  btnPropDetailsCard.setAttribute("href", "/property/" + markerLocation.id ); // link to the property detail page

  var pointCard = document.createElement("text"); // get coordinates for map -- do not display

  addressCard.id = markerLocation.id;
  //nameCard.textContent = markerLocation.name;
  priceTitleCard.textContent = "$ " + markerLocation.askingPrice;
  addressCard.textContent = markerLocation.address; // <span id={location.id}>{location.address}</span>
  cityCard.textContent = markerLocation.city;
  provinceCard.textContent = markerLocation.province;
  pointCard.textContent = markerLocation.point;

  // appending childrens
  divCard.appendChild(divCardContainer);
  divCardContainer.appendChild(imgPropCard);
  divCardContainer.appendChild(divBodyCard);

  divBodyCard.appendChild(priceTitleCard);
  divBodyCard.appendChild(addressCard);
  divBodyCard.appendChild(cityCard);
  divBodyCard.appendChild(provinceCard);
  //divBodyCard.appendChild(pointCard);
  //divBodyCard.appendChild(document.createElement("br"));
  divBodyCard.appendChild(btnPropDetailsCard);

  // divCard.appendChild(document.createElement("br"));

  divCardContainer.addEventListener(
    "mouseover",
    function (event) {
      marker.setOpacity(1);

      map.setCenter(markerLocation.point);
      map.setZoom(12);

      //divCardContainer.style.backgroundColor = "pink"
    },
    false
  );

  divCardContainer.addEventListener(
    "mouseout",
    function () {
      marker.setOpacity(0.6);

      //divCardContainer.style.backgroundColor = "white"
    },
    false
  );
}

function downloadUrl(url, callback) {
  var request = window.ActiveXObject
    ? new ActiveXObject("Microsoft.XMLHTTP")
    : new XMLHttpRequest();
  // request.setRequestHeader('Content-Type', 'text/xml');

  request.onreadystatechange = function () {
    if (request.readyState == 4) {
      callback(request, request.status);
    }
  };

  request.open("GET", url, true);
  request.send(null);
}
