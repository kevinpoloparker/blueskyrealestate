<?php
require_once 'vendor/autoload.php';
// Login page
 $passwordPepper = 'mmyb7oSAeXG9DTz2uFqu';

// state 1: first display
$app->get('/login',function($request, $response, $args){
    return $this->view->render($response,'login.html.twig');
});

// state 2 & 3: receiving submission
$app->post('/login',function($request, $response, $args) use($log) {
    $email = $request->getParam('email');
    $password = $request->getParam('password');

    $record=DB::queryFirstRow("select userID,email,password,firstName,lastName,phoneNum,isAdmin from user where email=%s",$email);
    $loginSuccess=false;
    if($record){
        global $passwordPepper;
        $pwdPeppered=hash_hmac("sha256",$password,$passwordPepper);
        $pwdHashed=$record['password'];
        
        if(password_verify($pwdPeppered,$pwdHashed)){
            $loginSuccess=true;
        }    
    }
    if(!$loginSuccess){
        $log->info(sprintf("Login failed for email %s from %s ", $email, $_SERVER['REMOTE_ADDR']));
        return $this->view->render($response,'login.html.twig',['error' => true]);
    }else{
        unset($record['password']);
        $_SESSION['loginUser'] = $record;
        $log->debug(sprintf("Login successful for email %s, uid=%d, from %s", $email, $record['userID'], $_SERVER['REMOTE_ADDR']));
        return $this->view->render($response, 'login_success.html.twig', ['userSession' => $_SESSION['loginUser'] ] );
    }
}); 
// login end

// Register start

// STATE 1: first display
$app->get('/register', function ($request, $response, $args) {
    return $this->view->render($response, 'register.html.twig');
});

// STATE 2&3: receiving submission
$app->post('/register', function ($request, $response, $args) {
    
    $email = $request->getParam('email');
    $firstname = $request->getParam('firstname');
    $lastname = $request->getParam('lastname');
    $phone = $request->getParam('phone');
    $isadmin = $request->getParam('isadmin') ?? '0';
    $pass1 = $request->getParam('password_1');
    $pass2 = $request->getParam('password_2');
    //

    $errorList = array();
    //$result = verifyUserName($name);
    //if ($result !== TRUE) { $errorList[] = $result; }
    
    // verify email
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
        $errorList [] =  "Email does not look valid" ;
        $email = "";
    } else {
        // is email already in use?
        $record = DB::queryFirstRow("SELECT userID FROM user WHERE email=%s", $email);
        if ($record) {
            $errorList [] =  "This email is already registered";
            $email = "";
        }
    }
    //
    $result = verifyPasswordQuailty($pass1, $pass2);
    if ($result != TRUE) { $errorList[] = $result; }
    //
    if ($errorList) { // STATE 3: errors
        return $this->view->render($response, 'register.html.twig',
                [ 'errorList' => $errorList, 'v' => ['firstname' => $firstname, 'email' => $email ]  ]);
    } else { // STATE 2: all good
       
        
        global $passwordPepper;
        $pwdPeppered = hash_hmac("sha256", $pass1, $passwordPepper);
        $pwdHashed = password_hash($pwdPeppered, PASSWORD_DEFAULT); // PASSWORD_ARGON2ID);*/

        DB::insert('user', ['email' => $email, 'password' => $pwdHashed,'firstname' => $firstname, 
                    'lastname' => $lastname,'phoneNum' =>$phone, 'isadmin' => $isadmin]);
        return $this->view->render($response, 'register_success.html.twig');
    }
});
// Register end

// used via AJAX ,email taken
$app->get('/isemailtaken/[{email}]', function ($request, $response, $args) {
    $email = isset($args['email']) ? $args['email'] : "";
    $record = DB::queryFirstRow("SELECT userID FROM user WHERE email=%s", $email);
    if ($record) {
        return $response->write("Email already in use ");
    } else {
        return $response->write("");
    }
});

// Logout
$app->get('/logout', function ($request, $response, $args) use ($log) {
    $log->debug(sprintf("Logout successful for uid=%d, from %s", @$_SESSION['loginUser']['userID'], $_SERVER['REMOTE_ADDR']));
    unset($_SESSION['loginUser']);
    return $this->view->render($response, 'logout.html.twig', ['userSession' => null ]);
});

// Reset Password
$app->get('/passreset_request',function($request, $response){
    return $this->view->render($response, 'password_reset.html.twig'); 
 });
 
 $app->post('/passreset_request',function($request, $response){
     $email = filter_var($_POST['email'], FILTER_VALIDATE_EMAIL);
     $user = DB::queryFirstRow("SELECT * FROM user WHERE email=%s",$email);
     print_r($email);
     if($user){
         $secret= generateRandomString(60);
         $datetime=gmdate("Y-m-d H:i:s");
 
         DB::insertUpdate('passwordreset',[
             'userID' => $user['userID'], 
             'secret' => $secret,
             'creationDateTime' => $datetime
         ],[ 'secret'=> $secret,
             'creationDateTime' => $datetime]
         );
         
         $to=$email;
         $subject= "Password reset";
         $emailBody =file_get_contents('templates/password_reset_email.html.strsub');
         $emailBody = str_replace('EMAIL' ,$email, $emailBody);
         $emailBody = str_replace('SECRET',$secret, $emailBody);
 
         //$message = $view -> render($response, 'password_reset_email.html.twig',['email' =>$email, 'secret' => $secret  ]);
         $headers = "MIME-Version: 1.0". "\r\n";
         $headers .= "Content-type:text/html;charset=UTF-8" ."\r\n";
         $headers .= "From: No Reaply <noreaply@blueskyrealestate.ipd24.ca>" ."\r\n";
         
         mail($to, $subject,$emailBody,$headers);
     }
     return $this->view->render($response, 'password_reset_sent.html.twig'); 
  });
 
  // password reset action
  $app->map(['GET','POST'],'/passresetaction/{secret}',function($request, $response, array $args){
      global $log;
      $secret=$args['secret'];
      $resetRecord= DB::queryFirstRow('SELECT * FROM passwordreset WHERE secret=%s', $secret);
      if(!$resetRecord){
          $log->debug(sprintf('password reset token not found, token=%s',$secret));
          return $this->view->render($response, 'password_reset_action_notfound.html.twig');
      }
      $creationDT=strtotime($resetRecord['creationDateTime']);
      $nowDT =strtotime(gmdate("Y-m-d H:i:s"));
      if($nowDT - $creationDT > 60*60){ // token exp
          DB::delete('passwordreset','secret=%s',$secret);
          $log->debug(sprintf('password reset token expired userid=%d,token =%s',$resetRecord['userID'],$secret));
          return $this->view->render($response, 'password_reset_action_notfound.html.twig');
      }
      if($_SERVER['REQUEST_METHOD'] == 'POST'){
      //if($request->getMethods() == 'POST'){
         $pass1=$_POST['password_1'];
         $pass2=$_POST['password_2'];
         $errorList =  array();
         if($pass1 != $pass2){
             $errorList[] = "Password don't match";
         }else{
             $passQuality = verifyPasswordQuailty($pass1,$pass2);
             if($passQuality != TRUE){
                 $errorList[] ="Password not Qualify";
             }
         }
         if($errorList){
             return $this->view->render($response, 'password_reset_action.html.twig',['errorList' => $errorList]);     
         }else{
            global $passwordPepper;
            $pwdPeppered = hash_hmac("sha256", $pass1, $passwordPepper);
            $pwdHashed = password_hash($pwdPeppered, PASSWORD_DEFAULT);
             DB::update('user',['password' => $pwdHashed], "userID=%d",$resetRecord['userID']);
             return $this->view->render($response, 'password_reset_action_success.html.twig');     
         }
 
      }else{
         return $this->view->render($response, 'password_reset_action.html.twig'); 
      }
  });
  