<?php

    session_start();
    require_once 'vendor/autoload.php';

    if (strpos($_SERVER['HTTP_HOST'], "ipd24.ca") !== false) {
        // hosting on ipd24.ca
        DB::$dbName = 'cp5003_blueskyrealestate';
        DB::$user = 'cp5003_blueskyrealestate';
        DB::$password = 'TM6xyR3wld9i';
    
    } else { // local computer 
        DB::$dbName = 'blueskyrealeastate';
        DB::$user = 'blueskyrealeastate';
        DB::$password = '!nZ*!PE98uoddLAr';
        DB::$host="localhost";   
        DB::$port = 3333;
    }
    
//Create and configure Slim app
$config = ['settings' => [
    'addContentLengthHeader' => false,
    'displayErrorDetails' => true
]];
$app = new \Slim\App($config);

// Fetch DI Container
$container = $app->getContainer();

// Register Twig View helper
$container['view'] = function ($c) {
    $view = new \Slim\Views\Twig(dirname(__FILE__) . '/templates', [
        'cache' => dirname(__FILE__) . '/tmplcache',
        'debug' => true, // This line should enable debug mode
    ]);
    //
    $view->getEnvironment()->addGlobal('test1','VALUE');
    // Instantiate and add Slim specific extension
    $router = $c->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    return $view;
};

