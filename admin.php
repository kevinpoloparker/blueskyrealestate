<?php

require_once 'utils.php';
// Admin
$app->get('/admin/users/list',function($request, $response, $args){
    $usersList= DB::query("SELECT * FROM user");
    return $this->view->render($response,'admin/users_list.html.twig',['usersList' => $usersList]);
});
// Admi users edit
$app->get('/admin/users/{op:edit|add}[/{userID:[0-9]+}]',function($request, $response, $args){

    if(($args['op'] == 'add' && !empty($args['userID'])) || ($args['op'] == 'edit' && empty($args['userID'])) ){
        $response = $response->withStatus(404);
        return $this->view->render($response,'admin/not_found.html.twig');
    }
    if($args['op'] == 'edit'){
        
        $user= DB::queryFirstRow("SELECT * FROM user WHERE userID=%d", $args['userID']);
        
            if(!$user){
            $response = $response->withStatus(404);
            return $this->view->render($response,'admin/not_found.html.twig');
            }
        }else{
            $user=[];
        }
        //print_r($user) ;   
    return $this->view->render($response,'admin/users_addedit.html.twig',['v' => $user ,'op' =>$args['op']]);
});
// STATE 2&3: receiving submission
$app->post('/admin/users/{op:edit|add}[/{userID:[0-9]+}]', function ($request, $response, $args) {    
    $op=$args['op'];
    //echo "Op = $op";
    if(($op == 'add' && !empty($args['userID'])) || ($op == 'edit' && empty($args['userID'])) ){
        $response = $response->withStatus(404);
        return $this->view->render($response,'admin/not_found.html.twig');
    }
    $email = $request->getParam('email');
    $firstname = $request->getParam('firstname');
    $lastname = $request->getParam('lastname');
    $phone = $request->getParam('phone');
    $isadmin = $request->getParam('isadmin') ?? '0';
    $pass1 = $request->getParam('password_1');
    $pass2 = $request->getParam('password_2');
    //
    
    $errorList = array();
    $result = verifyUserName($firstname);
    if ($result !== TRUE) { $errorList[] = $result; }
    
    // verify email
    if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE) {
        $errorList [] =  "Email does not look valid" ;
        $email = "";
    } else {
        // is email already in use?
        if($op == 'edit'){
            $record=DB::queryFirstRow("SELECT userID,isAdmin,email,password,firstName,lastName,phoneNum FROM user WHERE email =%s AND userID != %d",$email,$args['userID']);
            
        }else{
            $record=DB::queryFirstRow("SELECT userID,isAdmin,email,password,firstName,lastName,phoneNum FROM user WHERE email =%s ",$email);
        }
        //$record = DB::queryFirstRow("SELECT userID FROM user WHERE email=%s", $email);
        if ($record) {
            $errorList [] =  "This email is already registered";
            $email = "";
        }
    }
    if($op== 'add' || $pass1 !=''){
        $result = verifyPasswordQuailty($pass1, $pass2);
            if ($result != TRUE) 
                { 
                    $errorList[] = $result; 
                }
    }
    
    //
    if ($errorList) { // STATE 3: errors
        return $this->view->render($response, 'admin/users_addedit.html.twig',
                [ 'errorList' => $errorList, 'v' => ['firstName' => $firstname, 'email' => $email ]  ]);
    } else { // STATE 2: all good        
        if($op == 'add'){
            DB::insert('user', ['email' => $email, 'password' => $pass1,'firstName' => $firstname, 
                    'lastName' => $lastname,'phoneNum' =>$phone, 'isAdmin' => $isadmin]);
        //echo "Data inserted";
            return $this->view->render($response, 'admin/users_addedit_success.html.twig',['op' => $op]);
        }else{
            global $passwordPepper;
            $pwdPeppered = hash_hmac("sha256", $pass1, $passwordPepper);
            $pwdHashed = password_hash($pwdPeppered, PASSWORD_DEFAULT); 
            $data=['email' => $email, 'firstName' => $firstname,'lastName' => $lastname,'phoneNum' =>$phone, 'isAdmin' => $isadmin];
            //print_r($data);
            if($pass1 !=''){
                $data['password'] =$pwdHashed;
            }
        }        
        DB::update('user', $data, "userID=%d" ,$args['userID']);
        return $this->view->render($response, 'admin/users_addedit_success.html.twig',['op' =>$op]);
    }
});

// Delete user
$app->get('/admin/users/delete/{userID:[0-9]+}', function($request, $response, $args){
    $user=DB::queryFirstRow("SELECT userID,isAdmin,email,password,firstName,lastName,phoneNum FROM user WHERE userID=%d",$args['userID']);
    if(!$user){
        $response = $response->withStatus(404);
        return $this->view->render($response,'admin/not_found.html.twig');
    }
    //print_r($user);
    return $this->view->render($response, 'admin/users_delete.html.twig',['v' =>$user]);
});
// Post
$app->post('/admin/users/delete/{userID:[0-9]+}',function($request, $response, $args){
    DB::delete('user',"userID=%d", $args['userID']);
    return $this->view->render($response,'admin/users_delete_success.html.twig');
 });

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;

 // with given substring 
function startsWith($string, $startString) 
{ 
    $len = strlen($startString); 
    return (substr($string, 0, $len) === $startString); 
} 

// check for admin
$app->add(function (ServerRequestInterface $request, ResponseInterface $response, callable $next) {
    $url = $request->getUri()->getPath();
    if (startsWith($url, "/admin")) {
        if (!isset($_SESSION['loginUser']) || $_SESSION['loginUser']['isAdmin'] != 1) { // refuse if user not logged in AS ADMIN
            $response = $response->withStatus(403);
            return $this->view->render($response, 'admin/error_access_denied.html.twig');
        }
    }
    return $next($request, $response);
});