<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Property Detail</title>
    <link rel="stylesheet" href=”css/bootstrap.css”>
    <link rel="stylesheet" href=”css/bootstrap-responsive.css”>
    <link rel="stylesheet" href="styleloginreg.css">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>

<body>
    <div class="container mt-4">
        <div class="col-12 list-order">
            <div id="divWrapperPager" class="wrapper-pager">
                <ul class="pagination" role="navigation" justify-content="centered">
                    <li class="page-item">
                        <a class="page-link" href="#">Previous</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">1</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">2</a>
                    </li>
                    <li class="page-item">
                        <a class="page-link" href="#">3</a>
                    </li>
                    <li class="page-item">
                        <a class=page-link href="#">Next</a>
                    </li>
                </ul>
            </div>

        </div>
        <div class="card-deck mb-4">
            <div class="card" style="border-color: blue; border-radius: 4px; background-color: #F2FFFF;">

                <a href="/images/1/1big.jpg" alt="home big">
                    <img src="/images/1/1small.jpg" class="img-top" alt="home1">
                </a>
                <div class="card-body">
                    <h6>$449,000</h6>
                    <h6 class="card-title">House for sale</h6>
                    <div class="address">2665, Rue des Floralies
                        Vaudreuil-Dorion
                        Neighbourhood Vaudreuil East</div><br>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <a href="/images/2/2big.jpg" alt="home big">
                    <img src="/images/2/2small.jpg" class="img-top" alt="home1">
                </a>
                <div class="card-body">
                    <h6>$1,149,000</h6>
                    <h5 class="card-title">House for sale</h5>
                    <div class="address">4410, Rue Séguin<br>
                        Vaudreuil-Dorion
                        Neighbourhood Vaudreuil West</div><br>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <a href="/images/3/3big.jpg" alt="home big">
                    <img src="/images/3/3small.jpg" class="img-top" alt="home1">
                </a>
                <div class="card-body">
                    <h6>$610,000</h6>
                    <h5 class="card-title">House for sale</h5>
                    <div class="address">256, Rue Claude-Léveillée<br>
                        Vaudreuil-Dorion
                        Neighbourhood Vaudreuil East</div><br>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <a href="/images/4/4big.jpg" alt="home big">
                    <img src="/images/4/4small.jpg" class="img-top" alt="home1">
                </a>
                <div class="card-body">
                    <h6>$280,000</h6>
                    <h5 class="card-title">Condo for sale</h5>
                    <div class="address">734, Rue Valois, apt. 6<br>
                        Vaudreuil-Dorion
                        Neighbourhood Dorion </div><br>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
        </div>
        <div class="card-deck mb-4">
            <div class="card" style="width: 18rem;">
                <a href="/images/5/5big.jpg" alt="home big">
                    <img src="/images/5/5small.jpg" class="img-top" alt="home1">
                </a>
                <div class="card-body">
                    <h6>$695,000</h6>
                    <h5 class="card-title">House for sale</h5>
                    <div class="address">29, Rue Hazelwood<br>
                    Dollard-Des Ormeaux
                    Neighbourhood Central</div><br>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <a href="/images/6/6big.jpg" alt="home big">
                    <img src="/images/6/6small.jpg" class="img-top" alt="home1">
                </a>
                <div class="card-body">
                    <h6>$629,900</h6>
                    <h5 class="card-title">House for sale</h5>
                    <div class="address">19, Rue Oslo<br>
                    Dollard-Des Ormeaux
                    Neighbourhood Central</div><br>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <a href="/images/7/7big.jpg" alt="home big">
                    <img src="/images/7/7small.jpg" class="img-top" alt="home1">
                </a>
                <div class="card-body">
                    <h6>$1,850,000</h6>
                    <h5 class="card-title">House for sale</h5>
                    <div class="address">103, Rue Northview<br>
                    Dollard-Des Ormeaux
                    Neighbourhood Central</div><br>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
            <div class="card" style="width: 18rem;">
                <a href="/images/8/8big.jpg" alt="home big">
                    <img src="/images/8/8small.jpg" class="img-top" alt="home1">
                </a>
                <div class="card-body">
                    <h6>$699,000</h6>
                    <h5 class="card-title">House for sale</h5>
                    <div class="address">4, Rue Cedar<br>
                    Dollard-Des Ormeaux
                    Neighbourhood Central</div><br>
                    <a href="#" class="btn btn-primary">Go somewhere</a>
                </div>
            </div>
        </div>

    </div>
</body>

</html>