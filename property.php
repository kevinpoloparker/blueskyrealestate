<?php
require_once 'vendor/autoload.php';
require_once 'utils.php';

use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;
use Slim\Http\UploadedFile;

$container = $app->getContainer();
$container['upload_directory'] = __DIR__ . '/photo';

$app->get('/property/{id:[0-9]+}',function($request, $response, $args) {
    $propertyID = $args['id'];
    $property= DB::queryFirstRow("SELECT * FROM property WHERE propertyID= $propertyID");
    if($property==null){
        return $this->view->render($response, 'no_property.html.twig');
    }
    $photos= DB:: queryFirstColumn("SELECT photoPath FROM propertyphoto WHERE propertyID= $propertyID Order by isPrio ");
    $owner = false;
    if($_SESSION['loginUser']['userID']== $property['ownerID']){
        $owner=true;
    }

    return $this->view->render($response, 'property.html.twig',  ['owner'=>$owner, 'property' => $property, 'photos' => $photos  ]);
});

$app->post('/property/{id:[0-9]+}',function($request, $response, $args){

    if (!isset($_SESSION['loginUser'])){
        header("Location: \login");
        exit;
    }
    $propertyID = $args['id'];
    $property= DB::queryFirstRow("SELECT * FROM property WHERE propertyID= $propertyID");
    if($property==null){
        return $this->view->render($response, 'no_property.html.twig');
    }
    $photos= DB:: queryFirstColumn("SELECT photoPath FROM propertyphoto WHERE propertyID= $propertyID Order by isPrio ");
    $owner = false;
    if($_SESSION['loginUser']['userID']== $property['ownerID']){
        $owner=true;
    }
    $ownerID = DB::queryFirstField("SELECT ownerID FROM property WHERE propertyID= $propertyID");
    $ownerEmail = DB:: queryFirstField("SELECT email from user where userID=$ownerID");
    if($ownerEmail){
        $to = $ownerEmail;
        $subject = "Someone is requesting a visit at one of your properties.";
        $message = file_get_contents("templates/email_request_visit.strsub");
        $message = str_replace("ADDRESS",$property['address'],$message);
        $message = str_replace("FNAME",$_SESSION['loginUser']['firstName'],$message);
        $message = str_replace("LNAME",$_SESSION['loginUser']['lastName'],$message);
        $message = str_replace("EMAIL",$_SESSION['loginUser']['email'],$message);
        if($_SESSION['loginUser']['phoneNum']){
            $message = str_replace("PHONENUM",$_SESSION['loginUser']['phoneNum'],$message);
        }else{
            $message = str_replace("PHONENUM","",$message);
        }
        if($property['apartmentNum']){
            $message = str_replace("APT","apt# ".$property['apartmentNum'],$message);
        }else{
            $message = str_replace("APT","",$message);
        }
        $message = str_replace("CITY",$property['city'],$message);
        $message = str_replace("POSTAL",$property['postalCode'],$message);
        $message = str_replace("COUNTRY",$property['country'],$message);
        $headers = "MIME-Version: 1.0". "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" ."\r\n";
        $headers .= "From: No Reaply <noreaply@blueskyrealestate.ipd24.ca>" ."\r\n";
        mail($to,$subject,$message,$headers);
        setFlashMessage("Email sent.");
        return $this->view->render($response, 'property.html.twig',  ['owner'=>$owner, 'property' => $property, 'photos' => $photos  ]);
        
    }
});
$app->get('/addproperty',function($request, $response, $args){

    if (!isset($_SESSION['loginUser'])){
        header("Location: \login");
        exit;
    }
    return $this->view->render($response, 'addProperty.html.twig');
});
$app->post('/addproperty',function($request, $response, $args){
    if (!isset($_SESSION['loginUser'])){
        header("Location: \login");
        exit;
    }
    $address= $request->getParam('address');
    $apt= $request->getParam('aptNum');
    $city= $request->getParam('city');
    $province= $request->getParam('province');
    $country= $request->getParam('country');
    $postalCode= $request->getParam('postal');
    $propertyType= $request->getParam('typeOfHouse');
    $sellRent= $request->getParam('sellRent');
    $askingPrice= $request->getParam('askingPrice');
    $yearBuilt= $request->getParam('yearBuilt');
    $squareFoot= $request->getParam('squareFoot');
    $rooms= $request->getParam('rooms');
    $bedrooms= $request->getParam('bedrooms');
    $bathrooms= $request->getParam('bathrooms');
    $backyardSize= $request->getParam('backyardSize');
    $parking= $request->getParam('parking');
    $description= $request->getParam('description');
    $lng= $request->getParam('long');
    $lat= $request->getParam('lat');

    $directory = $this->get('upload_directory');
    $photo = $request->getUploadedFiles();

    $result = generalStringCheck("City",$city);
    if(!$result===false){
        $city="";
        $errorList['city'] = $result;
    }
    $result = generalStringCheck("Province",$province);
    if(!$result===false){
        $province="";
        $errorList['province'] = $result;
    }
    $result = generalStringCheck("Country",$country);
    if(!$result===false){
        $country="";
        $errorList['country'] = $result;
    }
    $result = postalCodeCheck(strtoupper ( $postalCode ));
    if(!$result===false){
        $errorList['postal'] = $result;
        $postalCode="";

    }
    $result= intCheck("Apartment number", $apt);
    if(!$result===false&&!empty($apt)){
        $apt="";
        $errorList['apt'] = $result;
    }
    $result= intCheck("Bedrooms", $bedrooms);
    if(!$result===false&&!empty($bedrooms)){
        $bedrooms="";
        $errorList['bedrooms'] = $result;
    }
    $result= intCheck("Bathrooms", $bathrooms);
    if(!$result===false&&!empty($bathrooms)){
        $bathrooms="";
        $errorList['bathrooms'] = $result;
    }
    $result= intCheck("Asking price", $askingPrice);
    if(!$result===false){
        $askingPrice="";
        $errorList['askingPrice'] = $result;
    }
    $result= intCheck("Property size", $yearBuilt);
    if(!$result===false||$yearBuilt>=date("Y")){
        $yearBuilt="";
        $errorList['yearBuilt'] = "The year built has to be a whole number and built this year or prior years.";
    }
    $result= intCheck("Property size", $squareFoot);
    if(!$result===false&&!empty($squareFoot)){
        $squareFoot="";
        $errorList['squareFoot'] = $result;
    }
    $result= intCheck("Backyard size", $backyardSize);
    if(!$result===false&&!empty($backyardSize)){
        $backyardSize="";
        $errorList['backyardSize'] = $result;
    }
    if(empty($propertyType)){
        $errorList['propertyType'] = "Please choose what type of property it is.";
    }
    if(empty($sellRent)){
        $errorList['sellRent'] = "Please choose if this property is to sell or to rent.";
    }
    if($lat===""){
        $errorList['address'] = "Please look over your address for any mistakes.";
    }
    if(empty($photo)){
        $errorList['photo'] = "Please choose at least 1 photo.";
    }

    if(isset($errorList)){
        $property['address'] =$address;
        $property['apartmentNum'] =$apt;
        $property['city'] =$city;
        $property['province'] =$province;
        $property['country'] =$country;
        $property['postalCode'] =$postalCode;
        $property['sellRent'] =$sellRent;
        $property['askingPrice'] =$askingPrice;
        $property['yearBuilt'] =$yearBuilt;
        $property['squareFoot'] =$squareFoot;
        $property['rooms'] =$rooms;
        $property['bedrooms'] =$bedrooms;
        $property['bathrooms'] =$bathrooms;
        $property['backyardSize'] =$backyardSize;
        $property['parking'] =$parking;
        $property['description'] =$description;
        $property['propertyType']= $propertyType;
        return $this->view->render($response, 'addProperty.html.twig',['property' => $property, 'errorList'=>$errorList]);
    }
    DB::insert('property', ['address' => $address, 'apartmentNum' => $apt,'city' => $city,'province' => $province, 'country' =>$country, 'postalCode' => $postalCode,
        'sellRent' =>$sellRent, 'askingPrice' => $askingPrice,'yearBuilt' =>$yearBuilt, 'squareFoot' => $squareFoot,'room' => $rooms,'bedroom' =>$bedrooms, 'bathroom' => $bathrooms,
        'backyardSize' =>$backyardSize, 'parking' => $parking,'description' =>$description, 'lng' => $lng,'lat' =>$lat,'ownerID' => $_SESSION['loginUser']['userID']]);
    
    $propertyID = DB::queryFirstField("SELECT propertyID FROM property ORDER BY propertyID DESC  ");
    $isPrio = $request->getParam("isPrio");
    $i = 0;
    foreach ($photo['photo'] as $uploadedFile) {
        if ($uploadedFile->getError() === UPLOAD_ERR_OK) {
            $filename = moveUploadedFile($directory, $uploadedFile);
            if($isPrio==$i){
                DB::insert('propertyphoto',['propertyID'=>$propertyID,'photoPath'=>$filename, 'isPrio'=>1]); 
            }else{
                DB::insert('propertyphoto',['propertyID'=>$propertyID,'photoPath'=>$filename, 'isPrio'=>0]);
            }
        }
        $i++;
    }
    setFlashMessage("Property Saved.");
    header("Location: /");
    exit;
});

$app->get('/editproperty/{id:[0-9]+}',function($request, $response, $args){
    $propertyID =$args["id"];
    $ownerID = DB::queryFirstField("SELECT ownerID FROM property WHERE propertyID= $propertyID");
    if (!isset($_SESSION['loginUser'])){
        header("Location: \login");
        exit;
    }
    if($ownerID===($_SESSION['loginUser'])){
        return $this->view->render($response, 'access_denied.html.twig');
    }
    $property= DB::queryFirstRow("SELECT * FROM property WHERE propertyID= $propertyID");
    if($property==null){
        return $this->view->render($response, 'no_property.html.twig');
    }
    $prioPhoto= DB::queryFirstField("SELECT photoPath FROM propertyPhoto WHERE propertyID= $propertyID && isPrio =1 ");
    $restPhoto = DB::query("SELECT photoPath FROM propertyPhoto WHERE propertyID= $propertyID && isPrio != 1 ");
    $photos[] = $prioPhoto;
    foreach ($restPhoto as $i => $value) {
        $photo[]= $restPhoto[$i];
    }
    return $this->view->render($response, 'addProperty.html.twig',[ 'property' => $property, 'pic' => $photos  ]);
});
$app->post('/editproperty/{id:[0-9]+}',function($request, $response, $args){
    $propertyID =$args["id"];
    $ownerID = DB::queryFirstField("SELECT ownerID FROM property WHERE propertyID= $propertyID");
    if (!isset($_SESSION['loginUser'])){
        header("Location: \login");
        exit;
    }
    if($ownerID===($_SESSION['loginUser'])){
        return $this->view->render($response, 'access_denied.html.twig');
    }
    $property= DB::queryFirstRow("SELECT * FROM property WHERE propertyID= $propertyID");
    if($property==null){
        return $this->view->render($response, 'no_property.html.twig');
    }
    $prioPhoto= DB::queryFirstField("SELECT photoPath FROM propertyPhoto WHERE propertyID= $propertyID && isPrio =1 ");
    $restPhoto = DB::query("SELECT photoPath FROM propertyPhoto WHERE propertyID= $propertyID && isPrio != 1 ");
    $photos[] = $prioPhoto;
    foreach ($restPhoto as $i => $value) {
        $photo[]= $restPhoto[$i];
    }
    return $this->view->render($response, 'addProperty.html.twig',[ 'property' => $property, 'pic' => $photos  ]);
});
