<?php
// db.php for database connection and initial code
require_once 'db.php';
require_once 'vendor/autoload.php';

use Respect\Validation\Validator as Validator;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Http\UploadedFile;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler(dirname(__FILE__) . '/logs/errors.log', Logger::ERROR));

DB::$error_handler = 'db_error_handler'; // runs on mysql query errors
DB::$nonsql_error_handler = 'db_error_handler'; // runs on library errors (bad syntax, etc)
/*
if (strpos($_SERVER['HTTP_HOST'], "ipd24.ca") !== false) {
    // hosting on ipd24.ca
    DB::$dbName = 'cp5003_blueskyrealestate';
    DB::$user = 'cp5003_blueskyrealestate';
    DB::$password = 'TM6xyR3wld9i';
} else { // local computer
    DB::$dbName = 'blueskyrealestate';
    DB::$user = 'blueskyrealestate';
    DB::$password = '!nZ*!PE98uoddLAr';
    DB::$port = 3333;
}*/

function db_error_handler($params) {
    global $log;
    // log first
    $log->error("Database error: " . $params['error']);
    if (isset($params['query'])) {
        $log->error("SQL query: " . $params['query']);
    }
    // redirect
    header("Location: /internalerror");
    die;
}

// All templates will be given userSession variable
$container['view']->getEnvironment()->addGlobal('userSession', $_SESSION['loginUser'] ?? null );
$container['view']->getEnvironment()->addGlobal('flashMessage', getAndClearFlashMessage());

// for login and register
require_once 'users.php';
// $passwordPepper = 'mmyb7oSAeXG9DTz2uFqu';

// for admin panel
require_once 'admin.php';

//for maps
require_once 'map.php';
require_once 'collection.php';
//for properties
require_once 'property.php';
// Chart display
$app->get('/chart',function($request, $response, $args){
    $result = DB::query('SELECT count(*) number ,propertyType FROM property GROUP BY propertytype');
    //print_r($result);
    return $this->view->render($response,'chart.html.twig',['list' => $result]);    
});

// ============= URL HANDLERS BELOW THIS LINE ========================
$app->get('/internalerror', function ($request, $response, $args) {
    return $this->view->render($response, 'error_internal.html.twig');
});
// Define app routes for index home page
$app->get('/home', function ($request, $response, $args) {
    return $this->view->render($response,'master.html.twig');    
});

// Define app routes for index home page
$app->get('/', function ($request, $response, $args) {
    //return $this->view->render($response,'master.html.twig');
    return $this->view->render($response,'index.html.twig');
});

$app->get('/agent', function ($request, $response, $args) {
    //return $this->view->render($response,'master.html.twig');
    return $this->view->render($response,'agent_det.html.twig');
});

// LOGIN / LOGOUT USING FLASH MESSAGES TO CONFIRM THE ACTION

function setFlashMessage($message) {
    $_SESSION['flashMessage'] = $message;
}

// returns empty string if no message, otherwise returns string with message and clears is
function getAndClearFlashMessage() {
    if (isset($_SESSION['flashMessage'])) {
        $message = $_SESSION['flashMessage'];
        unset($_SESSION['flashMessage']);
        return $message;
    }
    return "";
}

//function verifyPasswordQuailty($pass1, $pass2) {
// Moved in utils.php

// Run app
$app->run();
