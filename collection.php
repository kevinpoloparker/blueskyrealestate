<?php

// View (map)
$app->get('/collection', function ($request, $response, $args) {
    return $this->view->render($response, 'collection.html.twig');
});

// API endpoint (provide markers data points)
$app->get('/api/collection', function ($request, $response, $args) {
    //return $this->view->render($response, 'maps.html.twig');
    return $response->write(displaymapc(false))->withHeader("Content-Type", 'text/xml'); // header must be placed here because of slim syntax
});

// endpoint properties for RENT
$app->get('/api/collection/rent', function ($request, $response, $args) {
    return $response->write(displaymapc(true))->withHeader("Content-Type", 'text/xml'); // header must be placed here because of slim syntax
});

function parseToXMLc($htmlStr)
{
    $xmlStr = str_replace('<', '&lt;', $htmlStr);
    $xmlStr = str_replace('>', '&gt;', $xmlStr);
    $xmlStr = str_replace('"', '&quot;', $xmlStr);
    $xmlStr = str_replace("'", '&#39;', $xmlStr);
    $xmlStr = str_replace("&", '&amp;', $xmlStr);
    return $xmlStr;
}



function displaymapc($isRent)
{
    if ($isRent == true){

        $result = DB::query("SELECT * FROM property WHERE sellRent = 'Rent'");
    } 

    else {
    $result = DB::query("SELECT * FROM property WHERE sellRent = 'Sell'");
    }
    // header("Content-type: text/xml"); ---> does not work here because of slim syntax

    // Start XML file, echo parent node
    echo '<markers>';
    $ind = 0;
    // Iterate through the rows, printing XML nodes for each

    foreach ($result as $row) {
        // Add to XML document node
        echo '<marker ';
        echo 'id="' . $row['propertyID'] . '" ';
        echo 'address="' . parseToXMLc($row['address']) . '" ';
        echo 'lat="' . $row['lat'] . '" ';
        echo 'lng="' . $row['lng'] . '" ';
        echo 'propertyType="' . $row['propertyType'] . '" ';
        echo 'askingPrice="' . $row['askingPrice'] . '" '; 
        echo 'city="' . parseToXMLc($row['city']) . '" ';
        echo 'province="' . parseToXMLc($row['province']) . '" ';
        echo 'sellRent="' . $row['sellRent'] . '" ';
        echo '/>';
        $ind = $ind + 1;
    }

    // End XML file
    echo '</markers>';
}
